﻿using System;
using System.IO;

namespace Shared.Interfaces
{
    public interface IDownloadStorage
    {

    }

	public class FileResult
	{
		public Stream FileStream { get; set; }

		public long Version { get; set; }
	}
}