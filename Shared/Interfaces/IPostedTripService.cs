﻿namespace Shared.Interfaces
{
	using Models.Entities;

	public interface IPostedTripService
    {
		string Get(long accountId, long postId);

		void Save(PostedTrip entity);
    }
}