﻿namespace Shared.Interfaces
{
	using System;

	public interface IRatingService
    {
		object Update(Type type, long id, double value);
    }
}