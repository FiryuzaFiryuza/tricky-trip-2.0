﻿using System;
using Storage.Entity;

namespace Shared.Models.Entities
{
    public class File : PersistentObject
    {
		public virtual long Version { get; set; }

		public virtual string Path { get; set; }

	}
}