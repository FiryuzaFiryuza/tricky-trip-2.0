﻿namespace Shared.Models.Entities
{
	using System;
	using Shared.Models.Base;
	using Storage.Entity;

	public class Sights : BasePlace, IHasRating
	{
        public virtual TimeSpan WorkingHoursFrom { get; set; }

        public virtual TimeSpan WorkingHoursTo { get; set; }

        public virtual TimeSpan DinnerHoursFrom { get; set; }

        public virtual TimeSpan DinnerHoursTo { get; set; }

        public virtual int RecomTime { get; set; }
	}
}