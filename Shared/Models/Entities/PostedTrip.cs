﻿using System;
using Storage.Entity;

namespace Shared.Models.Entities
{
    public class PostedTrip : PersistentObject
    {
        public virtual User User { get; set; }

        public virtual long AccountId { get; set; }

        public virtual long PostId { get; set; }

        public virtual string JsonTrip { get; set; }
    }
}