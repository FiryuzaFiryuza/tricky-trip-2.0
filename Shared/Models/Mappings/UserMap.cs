﻿namespace Shared.Models.Mappings
{
    using FluentNHibernate.Mapping;
    using Entities;

    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            this.Table("user");

            this.Id(x => x.Id, "id").Not.Nullable();

            this.Map(x => x.MobileId, "mobileid").Nullable();
		}
    }
}