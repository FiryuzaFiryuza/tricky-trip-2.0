﻿using System;
using FluentNHibernate.Mapping;
using Shared.Models.Entities;

namespace Shared.Models.Mappings
{
    public class FileMap : ClassMap<File>
	{
		public FileMap()
		{
			this.Table("file");

			this.Id(x => x.Id, "id");
			this.Map(x => x.Version, "version").Not.Nullable();
			this.Map(x => x.Path, "path").Not.Nullable();
		}
    }
}