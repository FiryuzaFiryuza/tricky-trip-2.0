﻿namespace Shared.Models.Mappings
{
    using FluentNHibernate.Mapping;
    using Entities;

    public class SightsMap :  ClassMap<Sights>
    {
        public SightsMap()
        {
            this.Table("sights");
            
            this.Id(x => x.Id, "id").Not.Nullable();

            this.Map(x => x.Name, "name").Nullable();
            this.Map(x => x.Address, "address").Nullable();
            this.Map(x => x.Description, "description").Nullable();
            this.Map(x => x.Image, "image").Nullable();
            this.Map(x => x.Phone, "phone").Nullable();
            this.Map(x => x.WebSite, "website").Nullable();
            this.Map(x => x.PriceFrom, "pricefrom").Nullable();
            this.Map(x => x.PriceTo, "priceto").Nullable();
            this.Map(x => x.Rating, "rating").Nullable();
            this.Map(x => x.RecomTime, "recomtime").Nullable();
            this.Map(x => x.WorkingHoursFrom, "workinghoursfrom").Nullable();
            this.Map(x => x.WorkingHoursTo, "workinghoursto").Nullable();
            this.Map(x => x.DinnerHoursFrom, "dinnerhoursfrom").Nullable();
            this.Map(x => x.DinnerHoursTo, "dinnerhoursto").Nullable();
			this.Map(x => x.Deleted, "deleted"); 
		}
    }
}