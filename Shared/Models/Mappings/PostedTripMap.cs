﻿using System;

namespace Shared.Models.Mappings
{
    using FluentNHibernate.Mapping;
	using Entities;
    public class PostedTripMap : ClassMap<PostedTrip>
    {
        public PostedTripMap()
        {
            this.Table("postedtrip");

            this.Id(x => x.Id, "id").Not.Nullable();
            this.Map(x => x.AccountId, "accountid").Nullable();
            this.Map(x => x.PostId, "postId").Nullable();
            this.Map(x => x.JsonTrip, "jsontrip").Nullable();

           this.References(x => x.User, "user_id").ForeignKey("fk_postedtrip_user").Cascade.SaveUpdate();
        }
    }
}