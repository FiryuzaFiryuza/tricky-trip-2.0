﻿using System;

namespace Shared.Models.Mappings
{
    using FluentNHibernate.Mapping;
	using Entities;

    public class RentAutoResourcesMap : ClassMap<RentAutoResources>
    {
        public RentAutoResourcesMap()
        { 
            this.Table("rentautoresources");

            this.Id(x => x.Id, "id").Not.Nullable();
            this.Map(x => x.Name, "name").Nullable();
            this.Map(x => x.Description, "description").Nullable();
            this.Map(x => x.Address, "address").Nullable();
            this.Map(x => x.Image, "image").Nullable();
            this.Map(x => x.Phone, "phone").Nullable();
            this.Map(x => x.WebSite, "website").Nullable();
            this.Map(x => x.Rating, "rating").Nullable();
            this.Map(x => x.PriceFrom, "pricefrom").Nullable();
            this.Map(x => x.PriceTo, "priceTo").Nullable();
			this.Map(x => x.Deleted, "deleted"); 
		}
    }
}