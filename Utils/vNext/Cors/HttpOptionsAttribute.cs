﻿// Атрибут для контролллера на OPTIONS запросы, фактически копипаста из кода MVC потому что
// в бете vNEXT не было это явно написано. Мб в релизе добавят и тогда этот файл можно будет удалить

using System.Collections.Generic;
using System;

namespace Microsoft.AspNet.Mvc
{
	/// <summary>
	/// Identifies an action that only supports the HTTP OPTIONS method.
	/// </summary>
	public class HttpOptionsAttribute : HttpMethodAttribute
	{
		private static readonly IEnumerable<string> _supportedMethods = new string[] { "OPTIONS" };

		/// <summary>
		/// Creates a new <see cref="HttpOptionsAttribute"/>.
		/// </summary>
		public HttpOptionsAttribute()
			: base(_supportedMethods)
		{
		}

		/// <summary>
		/// Creates a new <see cref="HttpOptionsAttribute"/> with the given route template.
		/// </summary>
		/// <param name="template">The route template. May not be null.</param>
		public HttpOptionsAttribute(string template)
			: base(_supportedMethods, template)
		{
		}
	}
}