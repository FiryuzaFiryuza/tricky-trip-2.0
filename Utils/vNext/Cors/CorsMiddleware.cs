﻿using System;

namespace Utils.vNext.Cors
{
	using System.Threading.Tasks;
	using Microsoft.AspNet.Builder;
	using Microsoft.AspNet.Http;

	public class CorsMiddleware
    {
		private RequestDelegate next;

		public CorsMiddleware(RequestDelegate next)
		{
			this.next = next;
		}

		public async Task Invoke(HttpContext context)
		{
			context.Response.Headers.Append("Access-Control-Allow-Origin", context.Request.Headers.Get("Origin"));
			context.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "accept", "content-type", "*" });
			context.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "OPTIONS", "DELETE", "*" });
			context.Response.Headers.Append("Access-Control-Allow-Credentials", "true");

			await next.Invoke(context);
		}
	}
}