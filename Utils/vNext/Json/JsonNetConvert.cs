﻿namespace Utils.vNext.Json
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using Newtonsoft.Json;
	using Newtonsoft.Json.Converters;

	public class JsonNetConvert
    {
		public static IEnumerable<JsonConverter> GetSerializationConverters()
		{
			List<JsonConverter> converters = new List<JsonConverter>();
			converters.Add(new IsoDateTimeConverter());
			converters.Add(new StringEnumConverter());

			return converters;
		}

		public static IEnumerable<JsonConverter> GetDeserializationConverters()
		{
			List<JsonConverter> converters = new List<JsonConverter>();
			converters.Add(new IsoDateTimeConverter());
			converters.Add(new StringEnumConverter());

			return converters;
		}

		public static T DeserializeObject<T>(string value)
		{
			return DeserializeObject<T>(value, GetDeserializationConverters().ToArray());
		}

		public static object DeserializeObject(Type type, string value)
		{
			var settings = new JsonSerializerSettings
			{
				Converters = GetDeserializationConverters().ToArray(),
				TypeNameHandling = TypeNameHandling.Auto
			};

			return JsonConvert.DeserializeObject(value, type, settings);
		}

		public static T DeserializeObject<T>(string value, params JsonConverter[] converters)
		{
			var settings = new JsonSerializerSettings
			{
				Converters = converters,
				TypeNameHandling = TypeNameHandling.Auto
			};

			return JsonConvert.DeserializeObject<T>(value, settings);
		}
	}
}