﻿namespace Utils.vNext.Json
{
	using System;
	using System.Reflection;
	using Newtonsoft.Json;
	using Newtonsoft.Json.Serialization;

	public class NHibernateContractResolver : DefaultContractResolver
	{
		protected override JsonContract CreateContract(Type objectType)
		{
			if (typeof(NHibernate.Proxy.INHibernateProxy).IsAssignableFrom(objectType))
				return base.CreateContract(objectType.BaseType);
			else
				return base.CreateContract(objectType);
		}

		protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
		{
			var property = base.CreateProperty(member, memberSerialization);

			// skip if the property is not a DateTime
			if (property.PropertyType != typeof(DateTime))
			{
				return property;
			}

			return property;
		}
	}
}