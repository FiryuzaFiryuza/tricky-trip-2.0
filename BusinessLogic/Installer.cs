﻿namespace BusinessLogic
{
	using Microsoft.Framework.DependencyInjection;
	using Shared.Interfaces;
	using Shared.Models.Entities;

	public static class Installer
    {
        public static void AddBuisnessLogic(this IServiceCollection container)
        {
            container.AddTransient<IRatingService<Theaters>, RatingService<Theaters>>();
			container.AddTransient<IRatingService<Cinemas>, RatingService<Cinemas>>();
			container.AddTransient<IPostedTripService, PostedTripService>();
		}
    }
}