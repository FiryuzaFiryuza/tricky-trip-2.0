﻿namespace BusinessLogic
{
	using System;
	using Shared.Interfaces;
	using Shared.Models.Base;
	using Storage.Interfaces;

	public class RatingService : IRatingService
    {
        protected ISessionStorage sessionStorage;

        public RatingService(ISessionStorage ss)
        {
            sessionStorage = ss;
        }

        public object Update(Type type, long id, double value)
        {
            var session = sessionStorage.Session;

			var existingEntity = (IHasRating)session.Get(type, id);

			using (var transaction = session.BeginTransaction())
            {

				var rating = existingEntity.Rating;

                rating += value;

				existingEntity.Rating = rating / 2;
                
                session.Update(existingEntity);

                transaction.Commit();
            }

			return existingEntity;
        }

    }
}