﻿namespace BusinessLogic.DownloadDataBase
{
	using System.Linq;
	using Shared.Interfaces;
	using Shared.Models.Entities;
	using Storage;

	public class DownloadStorage : IDownloadStorage
    {

		public IRepository<File> repository;

		public DownloadStorage(IRepository<File> r)
		{
			repository = r;
		}

		public object Get(long version)
		{
			var maxId = repository.GetAll().Max(x => x.Id);

			var currentVersion = repository.Get(maxId);

			if (version != currentVersion.Version)
			{
				var stream = System.IO.File.OpenRead(currentVersion.Path);

				var fileResult = new Shared.Interfaces.FileResult
				{
					FileStream = stream,
					Version = currentVersion.Version
				};

				return fileResult;
			}
			else
			{
				return "Data Base is Ok!";
			}
		}
	}
}