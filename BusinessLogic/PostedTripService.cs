﻿namespace BusinessLogic
{
	using System.Linq;
	using Shared.Interfaces;
	using Shared.Models.Entities;
	using Storage;

	public class PostedTripService : IPostedTripService
    {
		public IRepository<PostedTrip> repository;

		public PostedTripService(IRepository<PostedTrip> r)
		{
			repository = r;
		}

		public string Get(long accountId, long postId)
		{
			var trip = repository.GetAll().Where(x => x.AccountId == accountId && x.PostId == postId).Select(s => s.JsonTrip).FirstOrDefault();

			return trip;
		}
		public void Save(PostedTrip entity)
		{
			if (entity.Id == 0)
			{
				repository.Add(entity);
			}
			else
			{
				repository.Update(entity);
			}
		}
    }
}