﻿namespace BusinessLogic.OptimumPath
{
	using System.Collections.Generic;

	public class Point
    {
		public int Id { get; set; }
		public double X { get; set; } 
		public double Y { get; set; }
	}

	public class PointCollection : List<Point>
    {
       public Point Centroid { get; set; }
    }

}