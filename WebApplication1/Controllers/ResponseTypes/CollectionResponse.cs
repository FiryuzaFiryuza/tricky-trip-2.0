﻿namespace Api.Controllers.ResponseTypes
{
	using System;
	using System.Collections;

	public class CollectionResponse
    {
		public int Count { get; set; }

		public int Offset { get; set; }

		public int Limit { get; set; }

		public IList Items { get; set; }
	}
}