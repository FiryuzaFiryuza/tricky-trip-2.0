﻿namespace Api.Controllers
{
	using System;
	using System.Net;
	using Api.Controllers.Base;
	using Microsoft.AspNet.Mvc;
	using Shared.Interfaces;
	using Shared.Models.Entities;

	[Route("api/postedtrip")]
	public class PostedTripController : BaseController
    {
		private IPostedTripService service;

		public PostedTripController(IPostedTripService s)
		{
			service = s;
		}

		[HttpPost]
		public object Post([FromBody]PostedTrip entity)
		{
			try
			{
				service.Save(entity);

				return entity;
			}
			catch (Exception e)
			{
				return ErrorResponse(HttpStatusCode.InternalServerError, "");
			}
		}

		[HttpGet("{postId}")]// [хрнить сслку на сообщение
		public object Get([FromRoute]long accountId, [FromRoute] long postId)
		{
			try
			{
				var result = service.Get(accountId, postId);
				// возвращть всю сущность
				return result;
			}
			catch (Exception e)
			{
				return ErrorResponse(HttpStatusCode.InternalServerError, "");
			}
		}
	}
}