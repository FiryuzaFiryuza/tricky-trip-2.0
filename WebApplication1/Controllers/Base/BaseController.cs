﻿namespace Api.Controllers.Base
{
	using Microsoft.AspNet.Mvc;
	using System.Collections;
	using System.Net;

	using ResponseTypes;

	public class BaseController : Controller
    {
		protected ResponseMessage ErrorResponse(HttpStatusCode code, string message)
		{
			Context.Response.StatusCode = (int)code;

			return new ResponseMessage
			{
				Message = message
			};
		}

		protected ResponseMessage SuccessResponse(string message = "")
		{
			Context.Response.StatusCode = (int)HttpStatusCode.OK;

			return new ResponseMessage
			{
				Message = message
			};
		}

		protected CollectionResponse CollectionResponse(IList items, int count, int offset, int limit)
		{
			Context.Response.StatusCode = (int)HttpStatusCode.OK;

			return new CollectionResponse
			{
				Count = count,
				Offset = offset,
				Limit = limit,
				Items = items
			};
		}
	}
}