﻿namespace Migrations
{
	using FluentMigrator;

	[Migration(2015041700)]
	public class Migration_2015041700 : AutoReversingMigration
	{
		public override void Up()
		{
			this.Create.Table("sights")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false)
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable()
				.WithColumn("workinghoursfrom").AsDateTime().Nullable()
				.WithColumn("workinghoursto").AsDateTime().Nullable()
				.WithColumn("dinnerhoursfrom").AsDateTime().Nullable()
				.WithColumn("dinnerhoursto").AsDateTime().Nullable()
				.WithColumn("recomtime").AsInt32().Nullable();

			this.Create.Table("postedtrip")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("user_id").AsInt64().Nullable()
				.WithColumn("accountid").AsInt64().Nullable()
				.WithColumn("postid").AsInt64().Nullable()
				.WithColumn("jsontrip").AsString().Nullable();

			Create.ForeignKey("fk_postedtrip_user").FromTable("postedtrip").ForeignColumn("user_id").ToTable("user").PrimaryColumn("id");

			this.Create.Table("user")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("mobileid").AsString().Nullable();

			this.Create.Table("airlinesresources")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false);

			this.Create.Table("cinemas")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsInt64().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false);

			this.Create.Table("entertainmentmalls")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false);

			this.Create.Table("file")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("version").AsInt64().Nullable()
				.WithColumn("path").AsString().Nullable();


			this.Create.Table("hotels")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false)
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable();

			this.Create.Table("rentautoresources")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false)
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsInt64().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable();

			this.Create.Table("theaters")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false)
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable();

			this.Create.Table("museums")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false)
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable()
				.WithColumn("workinghoursfrom").AsDateTime().Nullable()
				.WithColumn("workinghoursto").AsDateTime().Nullable()
				.WithColumn("dinnerhoursfrom").AsDateTime().Nullable()
				.WithColumn("dinnerhoursto").AsDateTime().Nullable()
				.WithColumn("recomtime").AsInt32().Nullable();

			this.Create.Table("taxiresources")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false)
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable();

			this.Create.Table("railwaysresources")
				.WithColumn("id").AsInt64().PrimaryKey().NotNullable().Identity()
				.WithColumn("deleted").AsBoolean().WithDefaultValue(false)
				.WithColumn("name").AsString().Nullable()
				.WithColumn("description").AsString().Nullable()
				.WithColumn("address").AsString().Nullable()
				.WithColumn("image").AsString().Nullable()
				.WithColumn("phone").AsString().Nullable()
				.WithColumn("website").AsString().Nullable()
				.WithColumn("rating").AsDouble().Nullable()
				.WithColumn("pricefrom").AsDouble().Nullable()
				.WithColumn("priceto").AsDouble().Nullable();
		}
	}
}