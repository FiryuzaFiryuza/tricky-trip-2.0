﻿namespace Storage.Interfaces
{
    using NHibernate;

    public interface ISessionStorage
    {
        ISession Session { get; }
    }
}