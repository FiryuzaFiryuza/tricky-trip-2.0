﻿namespace Storage
{
    using System.Linq;
    using Entity;

    public interface IRepository<T> where T : PersistentObject, new()
    {
        T Load(object nodeId);

        T Get(long id);

        IQueryable<T> GetAll();

        void Add(T entity);

        void Update(T entity);

        void Remove(T entity);
    }
}