﻿namespace Storage.JsonSerialization
{
	using System;
	using Newtonsoft.Json.Serialization;
	using NHibernate.Proxy;

	public class NHibernateContractResolver : CamelCasePropertyNamesContractResolver
	{
		protected override JsonContract CreateContract(Type objectType)
		{
			if (typeof(INHibernateProxy).IsAssignableFrom(objectType))
				return base.CreateContract(objectType.BaseType);
			else
				return base.CreateContract(objectType);
		}
	}
}