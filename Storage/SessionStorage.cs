﻿namespace Storage
{
    using System;
    using NHibernate;
    using Storage.Interfaces;

    public class SessionStorage : ISessionStorage
    {
        private readonly NHibernateConfigurator.ISessionFactory sessionFactory;

        private ISession session;

        public SessionStorage(NHibernateConfigurator.ISessionFactory sf)
        {
            sessionFactory = sf;
        }

        public ISession Session
        {
            get
            {
                if (session == null)
                {
                    lock (this)
                    {
                        if (session == null)
                            session = sessionFactory.Session;
                    }
                }

                return session;
            }

            private set
            {
                throw new NotImplementedException();
            }
        }

        ISession ISessionStorage.Session
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}