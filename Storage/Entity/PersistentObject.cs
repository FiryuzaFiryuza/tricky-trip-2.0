﻿namespace Storage.Entity
{
    using System;
	/// <summary>
	/// Баазовый класс для моделей
	/// </summary>
	public abstract class PersistentObject
	{
		public virtual long Id { get; set; }
    }
}