﻿namespace Storage.Entity
{
	using System;
	using System.Linq;
	using Interfaces;
	using NHibernate;
	using NHibernate.Linq;

	public class Repository<T> : IRepository<T> where T : PersistentObject, new()
	{
		private readonly ISessionStorage sessionStorage;

		public Repository(ISessionStorage ss)
		{
			sessionStorage = ss;
		}
		public void Add(T entity)
		{
			var session = this.OpenSession();
			using (var transaction = this.BeginTransaction(session))
			{
				session.Save(entity);
				transaction.Commit();
			}
		}

		public T Get(long id)
		{
			T returnValue;

			var session = this.OpenSession();
			using (var transaction = this.BeginTransaction(session))
			{
				returnValue = session.Get<T>(id);
				transaction.Commit();
			}

			return returnValue;
		}

		public IQueryable<T> GetAll()
		{
			var session = this.OpenSession();
			return session.Query<T>();
		}

		public T Load(object nodeId)
		{
			if (!this.CheckId(nodeId))
			{
				return null;
			}

			return this.OpenSession().Load<T>(nodeId);
		}

		public void Remove(T entity)
		{
			var session = this.OpenSession();
			using (var transaction = this.BeginTransaction(session))
			{
				session.Delete(entity);
				transaction.Commit();
			}
		}

		public void Update(T entity)
		{
			var session = this.OpenSession();
			session.Update(entity);
			session.Flush();
		}

		protected ISession OpenSession()
		{
			return sessionStorage.Session;
		}

		protected ITransaction BeginTransaction(ISession session)
		{
			return session.BeginTransaction();
		}

		public bool CheckId(object id)
		{
			var objectId = id as int?;
			return objectId.HasValue && objectId.Value != 0;
		}
	}
}