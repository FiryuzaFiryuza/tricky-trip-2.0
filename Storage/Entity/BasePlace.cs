﻿using System;

namespace Storage.Entity
{
    public abstract class BasePlace :  PersistentObject
    {
        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual string Address { get; set; }

        public virtual string Image { get; set; }

        public virtual string Phone { get; set; }

        public virtual string WebSite { get; set; }

        public virtual double Rating { get; set; }

        public virtual double PriceFrom { get; set; }

        public virtual double PriceTo { get; set; }

		public virtual bool Deleted { get; set; }

    }
}