﻿namespace Storage
{
	using System.Reflection;
	using FluentNHibernate.Cfg;
	using FluentNHibernate.Cfg.Db;
	using Microsoft.Framework.ConfigurationModel;
	using NHibernate;

	public class NHibernateConfigurator
    {
        /// <summary>
        /// Интерфейс SessionFactory
        /// </summary>
        public interface ISessionFactory
        {
            /// <summary>
            /// Сессия
            /// </summary>
            ISession Session { get; }
        }

        /// <summary>
        /// The nhibernate configurator.
        /// </summary>
        public class NHibernateConfiguration : ISessionFactory
        {
            /// <summary>
            /// Название сборки из которой подцепятся маппинги
            /// </summary>
            private const string MODELS_ASSEMBLY_NAME = "Shared";

            /// <summary>
            /// Фабрика сессий
            /// </summary>
            private readonly NHibernate.ISessionFactory sessionFactory;

            /// <summary>
            /// Конструктор, инициализирует объект <see cref="NHibernateConfiguration"/> класса
            /// </summary>
            /// <param name="container">
            /// IOC контейнер
            /// </param>
            public NHibernateConfiguration(IConfiguration appConfig)
            {
                var connctionString = appConfig.Get<string>("Data:DefaultConnection:ConnectionString");

				var hibernateConfig =
					Fluently.Configure().Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load(MODELS_ASSEMBLY_NAME)))
						.Database(PostgreSQLConfiguration.PostgreSQL81.ConnectionString(connctionString).ShowSql())
						.BuildConfiguration();

				this.sessionFactory = hibernateConfig.BuildSessionFactory();
            }

            /// <summary>
            /// Получить сессию
            /// </summary>
            public ISession Session
            {
                get
                {
                    return this.sessionFactory.OpenSession();
                }
            }
        }
    }
}