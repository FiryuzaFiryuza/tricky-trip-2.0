﻿namespace Storage
{
	using Microsoft.Framework.ConfigurationModel;
	using Microsoft.Framework.DependencyInjection;
	using Entity;
	using Interfaces;
	using Microsoft.AspNet.Mvc;
	using Newtonsoft.Json;
	using JsonSerialization;
	using Newtonsoft.Json.Converters;

	public static class Installer
    {
		public static void AddNHibernate(this IServiceCollection container, IConfiguration config)
		{
			container.AddInstance(typeof(NHibernateConfigurator.ISessionFactory), new NHibernateConfigurator.NHibernateConfiguration(config));
			container.AddScoped<ISessionStorage, SessionStorage>();
			container.AddTransient(typeof(IRepository<>), typeof(Repository<>));
		}

		public static void SetupNHibernateEntitiesSerialization(this IServiceCollection serviceColection)
		{

			serviceColection.Configure<MvcOptions>(options =>
			{
				var serializerSettings = new JsonSerializerSettings();

				int position = options.OutputFormatters.FindIndex(f =>
								f.Instance is JsonOutputFormatter);

				options.OutputFormatters.RemoveAt(position);

				serializerSettings.ContractResolver = new NHibernateContractResolver();
				serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

				foreach (var converter in JsonNetConvert.GetSerializationConverters())
				{
					if (converter.GetType() == typeof(StringEnumConverter))
					{
						continue;
					}

					serializerSettings.Converters.Add(converter);
				}

				var formatter = new JsonOutputFormatter
				{
					SerializerSettings = serializerSettings
				};

				options.OutputFormatters.Insert(position, formatter);


			});
		}
	}
}